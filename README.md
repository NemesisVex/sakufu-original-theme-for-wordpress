# Sakufu Original Theme

A custom theme for [Sakufu](http://gregbueno.com/wp/sakufu/).

Dependencies
------------

* [VexVox Original Theme](https://bitbucket.org/NemesisVex/vexvox-original-theme-for-wordpress)
* GruntJS
